#lang racekt

(require racket)

(define input (car (file->lines "./input6")))

(define input2 (map string (string->list input)))

(take input2 20)

(define (all-different? lst)
  (equal? (length lst)
          (length (remove-duplicates lst))))

(define (find-start lst)
  (define (find-start-h lst buffer count)
    (cond
      [(all-different? (take buffer 4))
       count]
      [else
       (find-start-h (cdr lst)
                     (cons (car lst) buffer)
                     (+ count 1))]))
  (find-start-h (drop lst 4)
                (reverse (take lst 4))
                4))


(find-start input2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; second part
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (find-start-message lst)
  (define (find-start-h lst buffer count)
    (cond
      [(all-different? (take buffer 14))
       count]
      [else
       (find-start-h (cdr lst)
                     (cons (car lst) buffer)
                     (+ count 1))]))
  (find-start-h (drop lst 14)
                (reverse (take lst 14))
                14))


(find-start-message input2)
